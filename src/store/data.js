/* eslint-disable */
import axios from 'axios'

export default {
state: {
    data: 0
  },
  mutations: {
    setData(state,data) {
        state.data = data;
        console.log(state.data);
    }
},
getters: {
    data: state => state.data
  }
}