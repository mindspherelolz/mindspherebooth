/* eslint-disable */
import axios from 'axios'

export default {
state: {
    token: ''
  },
  mutations: {
    async getToken(state) {
        let res = await axios.get('https://semens-practice.herokuapp.com/token');
        state.token = res.data;
        console.log('token: ' + state.token);
    }
},
getters: {
    token: state => state.token
  }
}