import Vue from 'vue';
import Vuex from 'vuex';

import layout from './layout';
import events from './events';
import token from './token';
import data from './data';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    events,
    token,
    data
  },
});
