import Vue from 'vue';
import Router from 'vue-router';

import Layout from '@/components/Layout/Layout';
import Typography from '@/pages/Typography/Typography';
import Tables from '@/pages/Tables/Tables';
import Notifications from '@/pages/Notifications/Notifications';
import Icons from '@/pages/Icons/Icons';
import Maps from '@/pages/Maps/Maps';
import Charts from '@/pages/Charts/Charts';
import Dashboard from '@/pages/Dashboard/Dashboard';
import Login from '@/pages/Login/Login';
import ErrorPage from '@/pages/Error/Error';
import Maintenance from '@/pages/AnotherPage/Maintenance'
import Chart from '@/pages/AnotherPage/Charts'
import ONS1510 from '@/pages/AnotherPage/ONS-1510'
import ONS3020 from '@/pages/AnotherPage/ONS-3020'
import ONS5035 from '@/pages/AnotherPage/ONS-5035'


Vue.use(Router);

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/error',
      name: 'Error',
      component: ErrorPage,
    },
    {
      path: '/app',
      name: 'Layout',
      component: Layout,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
        },
        {
          path: 'charts',
          name: 'Charts',
          component: Chart,
        },
        {
          path: 'maintenance',
          name: 'Maintenance',
          component: Maintenance,
        },
        {
          path: 'Orisol/ONS1510',
          name: 'ONS1510',
          component: ONS1510,
        },
        {
          path: 'Orisol/ONS3020',
          name: 'ONS3020',
          component: ONS3020,
        },
        {
          path: 'Orisol/ONS5035',
          name: 'ONS5035',
          component: ONS5035,
        },
        {
          path: 'typography',
          name: 'Typography',
          component: Typography,
        },
        {
          path: 'tables',
          name: 'Typography',
          component: Tables,
        },
        {
          path: 'notifications',
          name: 'Notifications',
          component: Notifications,
        },
        {
          path: 'components/icons',
          name: 'Icons',
          component: Icons,
        },
        {
          path: 'components/maps',
          name: 'Maps',
          component: Maps,
        },
        {
          path: 'components/charts',
          name: 'Charts',
          component: Charts,
        },
      ],
    },
    {
      path: '*',
      name: 'Error',
      component: ErrorPage,
    }
  ],
});
