// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'expose-loader?jQuery!jquery' // eslint-disable-line
import 'expose-loader?$!jquery' // eslint-disable-line
import 'vue-material/dist/vue-material.min.css'
import 'leaflet/dist/leaflet.css';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VCalendar from 'v-calendar';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueMaterial from 'vue-material'
import BlockUI from 'vue-blockui'

import store from './store';
import router from './Routes';
import App from './App';
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
Vue.use(VueMaterial);
Vue.use(BlockUI);
/* eslint-disable */
/* leaflet icon */
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('../src/assets/logos/automation_logo_3997.png'),
  // iconUrl: require('leaflet/dist/images/marker-icon.png'),
  iconUrl: require('../src/assets/logos/automation_logo_3997.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});


Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB7OXmzfQYua_1LEhRdqsoYzyJOPh9hGLg',
  },
});

Vue.use(VCalendar, {
  firstDayOfWeek: 2,  // Monday
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
});
